class GameFacade
  attr_reader :response

  def initialize(game)
    @game = game
    @response = build_response
  end

  def get_player_score(player)
    player.plays.inject(0){ |total, play| total += play.score }
  end

  def build_response
    { game_id: @game.id,
         scores: [
           {
            user_id: @game.player_1.id,
            score: get_player_score(@game.player_1)
           },
           {
            user_id: @game.player_2.id,
            score: get_player_score(@game.player_2)
            }
          ]
    }
  end



end
