class RootFacade
  attr_reader :word,
              :sentences

  def initialize(response, word)
    @response = response
    @word = word
    @sentences = []
    build_sentences
  end

  def parsed_response
    JSON.parse(@response.body, symbolize_names: true)
  end

  def isolate_sentences
    parsed_response[:results].first[:lexicalEntries].first[:sentences]
  end

  def isolate_regions
    isolate_sentences.select do |entry|
      entry[:regions].first == 'British' || entry[:regions].first == 'Canadian'
    end
  end

  def build_sentences
    isolate_regions.each do |sentence|
      @sentences << Sentence.new(sentence)
    end
  end
end
