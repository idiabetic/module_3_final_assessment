module Api
  module V1
    class PlaysController < ActionController::API
      def create
        game = Game.find(params[:game_id])
        user = User.find(params[:user_id])
        render json: Play.create(game: game, user: user, word: params[:word]), status: 201

      end
    end
  end
end
