module Api
  module V1
    class GamesController < ActionController::API
      def show
        game = Game.find(params[:id])
        facade = GameFacade.new(game)
        render status: 404 unless game
        render json: facade.response
      end
    end
  end
end
