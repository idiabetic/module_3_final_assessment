class WelcomeController < ApplicationController
  def index
    if params[:word]
        dict = DictionaryInterface.new(params[:word]).response
        @root_facade = RootFacade.new(dict, params[:word])
    end
  end
end
