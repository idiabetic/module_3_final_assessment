class Sentence
  attr_reader :region,
              :text

  def initialize(sentence)
    @region   = sentence[:regions].first
    @text = sentence[:text]
  end

end
