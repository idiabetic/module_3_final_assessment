class DictionaryInterface
  attr_reader :response

  def initialize(word)
    @word = word.downcase
    @response = perform_get
  end

  def conn
    Faraday.new(url: 'https://od-api.oxforddictionaries.com')
  end

  def build_url
    "/api/v1/entries/en/#{@word}/sentences"
  end

  def perform_get
    conn.get do |req|
      req.headers['app_id'] =  ENV['DICT_API_ID']
      req.headers['app_key'] =  ENV['DICT_API_KEY']
      req.url build_url
    end
  end

end
