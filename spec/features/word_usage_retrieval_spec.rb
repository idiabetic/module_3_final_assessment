require 'rails_helper'

describe 'Root Page' do
  it 'Should return word usage when the user enters a word' do
    VCR.use_cassette('Mindfullness') do
    #As a guest user (no sign in required)

    #When I visit "/"
    visit '/'
    #And I fill in a text box with "mindfulness"
    fill_in :word, with: 'mindfulness'
   # And I click "Submit"
    click_on 'Submit'
    #Then I should see a message that says "Examples for using 'mindfulness'"
    expect(page).to have_content("Examples for using 'mindfulness'")
    #And I should see a list of sentences with examples of how to use the word
    expect(page).to have_content("Meditation is one way to express mindfulness in a dedicated, concentrated manner.")
    expect(page).to have_content("At such times, mindfulness of the practice of patience and the application of certain techniques will help us to continue generating this attitude.")
    expect(page).to_not have_content("Try to develop deep concentration through the awareness of the breath which is called the mindfulness of the breath or you can say mindfulness of the body.")
    #And I should see only sentences where the region for usage is "Brittish" or "Canadian"
    #And I should not see sentences for any other regions (or blank regions)
    end
  end
end
