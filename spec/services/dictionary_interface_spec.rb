require 'rails_helper'

describe 'Dictionary Service' do
  it 'should be able to make requests' do
    VCR.use_cassette('dictionary_interface_spec') do
      word = 'Mindfulness'
      dict = DictionaryInterface.new(word)
      expect(dict.response.status).to eq(200)

      parsed_response = JSON.parse(dict.response.body, symbolize_names: true)
      expect(parsed_response.keys.count).to eq(2)
    end
  end
end
