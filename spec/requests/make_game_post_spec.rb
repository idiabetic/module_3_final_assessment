require 'rails_helper'
  describe 'Game API' do
    it 'should make a play' do

     post "/api/v1/games/66/plays?user_id=1&word=at"

     expect(response.status).to eq(201)
      expected_response = {
  "game_id":66,
  "scores": [
    {
      "user_id":1,
      "score":17
    },
    {
      "user_id":2,
      "score":16
    }
  ]
}

     get("/api/v1/games/66")

     parsed = JSON.parse(response.body, symbolize_names: true)
     expect(parsed).to eq(expected_response)

    end
  end
